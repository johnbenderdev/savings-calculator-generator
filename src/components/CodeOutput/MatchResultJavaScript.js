//Currently experimenting on localhost with best way to render this code for the end user!

import React from "react";

const MatchResult = () => {
    return (
        <div>
            <p className="f5 lh-copy dark-gray fw3 f4-l">We can match your current rate!</p>
        </div>
    )
}

export default MatchResult;